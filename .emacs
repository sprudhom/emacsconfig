(load-theme 'wombat) 
(setq frame-title-format "emacs")

(menu-bar-mode -1)

(tool-bar-mode -1)
(scroll-bar-mode -1)

(set-default 'cursor-type 'hbar)

(ido-mode)

(column-number-mode)

(show-paren-mode)

(global-hl-line-mode)

(winner-mode t)

(windmove-default-keybindings)

(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/")
	     t)

(add-to-list 'package-archives
	     '("marmalade" . "http://marmalade-repo.org/packages/")
	     t)

(package-initialize)

(global-set-key (kbd "M-x") 'smex)

(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

(nlinum-mode)

(autopair-global-mode)

(ac-config-default)

;;;; Undo tree that can summon to move back to a point of time. INSTALL undo-tree-mode

(global-undo-tree-mode)

(global-set-key (kbd "M-/") 'undo-tree-visualize)

;;;; For time when we have a lot of windows open. INSTALL switch-window

(global-set-key (kbd "C-M-z") 'switch-window)

;;;; Being able to jump in the same window to different places with minimum key-strockes. INSTALL ace-jump-mode

(global-set-key (kbd "C->") 'ace-jump-mode)

(setq org-directory "C:\\Users\\utilisateur\\org")
(setq org-mobile-directory "C:\\Users\\utilisateur\\Dropbox\\org")
(setq org-agenda-files '("C:\\Users\\de8152\\Dropbox\\PROJETS\\Hydro-Qu�bec\\Notes\\ORG"))
(setq org-mobile-inbox-for-pull "C:\\Users\\utilisateur\\Dropbox\\org\\from-mobile.org")
(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "� FAIRE(a)" "NEXT(n)""SUIVANT(s)" "|" "DONE(d)""FAIT(f)")
              (sequence "WAITING(w@/!)" "EN ATTENTE(e@/!)" "HOLD(h@/!)""ARR�T�(r@/!)" "|" "CANCELLED(c@/!)""ANNUL�(n@/!)" "PHONE""T�L�PHONE"))))

(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
	      ("� FAIRE" :foreground "red" :weight bold)
              ("NEXT" :foreground "blue" :weight bold)
	      ("SUIVANT" :foreground "blue" :weight bold)
              ("DONE" :foreground "forest green" :weight bold)
	      ("FAIT" :foreground "forest green" :weight bold)
              ("WAITING" :foreground "orange" :weight bold)
	      ("EN ATTENTE" :foreground "orange" :weight bold)
              ("HOLD" :foreground "magenta" :weight bold)
	      ("ARR�T�" :foreground "magenta" :weight bold)
              ("CANCELLED" :foreground "forest green" :weight bold)
	      ("ANNUL�" :foreground "forest green" :weight bold)
	      ("PHONE" :foreground "forest green" :weight bold)
              ("T�L�PHONE" :foreground "forest green" :weight bold))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (switch-window undo-tree nlinum-relative auto-complete smex))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
